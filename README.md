# Hospital Management System

**Developer**
>*Manimaran.K*

**My Blog**
>[https://manimaran96.wordpress.com](https://manimaran96.wordpress.com)

**Project Abstract**

1. Hospital management system fully designed in Virtual Basic.
2. Store all details in data base.
3. New patient details regiter also.
4. Existing patient are check and view medicine details.
5. All reports are maintained in database
6. Easily add,remove patient details.

**How to use**
1. Install virtual basic
2. Open hospital.vbp file


**About me**

        Manimaran.K
        Computer Science Engineering
        manimaraninam1027@gmail.com
        Facebook - https://www.facebook.com/manimaran.cse.1
        Twitter - https://twitter.com/Manimaran_lpt
